package nl.thatzokay.main.main.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandCurrentTime implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (command.getName().equalsIgnoreCase("currenttime")){
            if (commandSender instanceof Player){
                Player player = (Player) commandSender;
                player.sendMessage("The time is: " + player.getWorld().getTime());
            }
        }
        return true;
    }
}
