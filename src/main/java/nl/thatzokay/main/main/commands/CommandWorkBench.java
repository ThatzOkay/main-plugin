package nl.thatzokay.main.main.commands;

import nl.thatzokay.main.main.Main;
import nl.thatzokay.main.main.Utils.API;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;

public class CommandWorkBench implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (command.getName().equalsIgnoreCase("workbench")){
            if (commandSender instanceof Player){
                API api = new API();
                Player player = (Player) commandSender;
                if (player.hasPermission("main.command.workbench")){
                    player.openInventory(Bukkit.createInventory(null, InventoryType.WORKBENCH));
                }else{
                    String message = Main.getInstance().connection.getMessage("NOPERMISSIONS");
                    player.sendMessage(api.format(message));
                }
            }
        }
        return true;
    }
}
