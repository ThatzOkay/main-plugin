package nl.thatzokay.main.main.config;

import nl.thatzokay.main.main.Main;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class ConfigManager {

    private String FileName;

    private File file;
    private YamlConfiguration configuration;

    private String path;

    private Main plugin;

    public ConfigManager(String path, String fileName, Main plugin){
        file = new File(plugin.getDataFolder() + "/" + path, fileName);
        if (!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        configuration = YamlConfiguration.loadConfiguration(file);
    }

    public File getFile() {
        return file;
    }

    public YamlConfiguration getConfiguration() {
        return configuration;
    }

    public void saveConfig(){
        try {
            getConfiguration().save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
