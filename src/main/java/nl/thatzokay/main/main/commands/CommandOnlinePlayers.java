package nl.thatzokay.main.main.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandOnlinePlayers implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (command.getName().equalsIgnoreCase("onlineplayers")){
            if (commandSender instanceof Player){
                for (Player player : Bukkit.getOnlinePlayers()) {
                    String playername = player.getName();
                    player.sendMessage(playername + "");
                }
            }
        }
        return true;
    }
}
