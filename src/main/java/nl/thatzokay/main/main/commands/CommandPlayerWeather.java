package nl.thatzokay.main.main.commands;

import nl.thatzokay.main.main.Main;
import nl.thatzokay.main.main.Utils.API;
import org.bukkit.WeatherType;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.List;

public class CommandPlayerWeather implements TabExecutor {

    public String[] weathers = {"clear", "rain", "reset"};

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (command.getName().equalsIgnoreCase("playerweather")){
            if (commandSender instanceof Player){
                Player player = (Player) commandSender;
                API api = new API();
                if (player.hasPermission("main.command.playerweather")|| player.isOp()){
                    if (strings.length == 0){

                    }
                    if (strings[0].equalsIgnoreCase("clear")){
                        player.setPlayerWeather(WeatherType.CLEAR);
                        player.sendMessage("Player weather set to clear");
                    }else if (strings[0].equalsIgnoreCase("rain")){
                        player.setPlayerWeather(WeatherType.DOWNFALL);
                        player.sendMessage("Player weather set to rain");
                    }else if(strings[0].equalsIgnoreCase("reset")){
                        player.sendMessage("Player weather reset");
                        player.resetPlayerWeather();
                    }
                }else{
                    String message = Main.getInstance().connection.getMessage("NOPERMISSIONS");
                    player.sendMessage(api.format(message));
                }
            }
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] strings) {
        return Arrays.asList(weathers);
    }
}
