package nl.thatzokay.main.main.commands;

import nl.thatzokay.main.main.Main;
import nl.thatzokay.main.main.Utils.API;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.logging.Level;

public class CommandMessage implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (command.getName().equalsIgnoreCase("message")){
            if (commandSender instanceof Player){
                API api = new API();
                Main main = Main.getInstance();
                Player player = (Player) commandSender;
                Player sendTo = Bukkit.getPlayer(strings[0]);
                String msg = "";

                if (strings.length == 0) {
                    player.sendMessage("§cusage: /msg [player] [message]");
                }

                for (int i = 1; i < strings.length; i++){
                    msg = msg + " " + strings[i];
                }

                if (player == sendTo){
                    player.sendMessage("§cYou can't write a message to yourself");
                }

                if (sendTo != null){
                    API.lastsend.put(sendTo.getUniqueId(), player.getUniqueId());
                    msg = api.format(msg);
                    main.getLogger().log(Level.INFO, API.lastsend.toString());
                    player.sendMessage(player.getDisplayName() + "§8 >> " + sendTo.getDisplayName() + "§8 >>§7" + msg);
                    sendTo.sendMessage("From: " + player.getDisplayName() + "§8 >>§7" + msg);
                }else{
                    String message = Main.getInstance().connection.getMessage("PLAYEROFFLINE").replace("%offline_player%", strings[0]);
                    player.sendMessage(api.format(message));
                }

            }
        }
        return true;
    }
}
