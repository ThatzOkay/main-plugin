package nl.thatzokay.main.main.events;

import nl.thatzokay.main.main.Main;
import nl.thatzokay.main.main.Utils.API;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class OnPlayerJoinEvent implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event){
        Main plugin = Main.getInstance();
        Player player = event.getPlayer();
        API api = new API();
        plugin.connection.createPlayer(player);
        plugin.connection.setOnlinePlayers();
        player.sendMessage("Online players: " + Bukkit.getOnlinePlayers().size());
        if (plugin.connection.playerExists(player)){
            player.sendMessage("Last join date was: " + plugin.connection.getLeaveDate(player));
        }else{
            if (plugin.connection.isSpawnLocationSet()){
                player.teleport(plugin.connection.getSpawnLocation());
            }else{
                player.teleport(player.getWorld().getSpawnLocation());
            }
        }
    }
}
