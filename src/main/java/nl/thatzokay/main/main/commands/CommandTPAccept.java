package nl.thatzokay.main.main.commands;

import nl.thatzokay.main.main.Main;
import nl.thatzokay.main.main.Utils.API;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.logging.Level;

public class CommandTPAccept implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (command.getName().equalsIgnoreCase("tpaccept")){
            if (commandSender instanceof Player){
                Player player = (Player) commandSender;
                player.sendMessage(API.lasttpa.values().toString());
                if (!API.lasttpa.containsKey(player.getUniqueId())){
                    player.sendMessage("You do not have a tpa request");
                    return true;
                }
                Player teleportAccepted = Bukkit.getPlayer(API.lasttpa.get(player.getUniqueId()));
                player.sendMessage(teleportAccepted.getUniqueId().toString());
                if (teleportAccepted == null){
                    player.sendMessage("The player has gone offline");
                    API.lasttpa.remove(player.getUniqueId());
                }

                teleportAccepted.sendMessage(player.getDisplayName() + " has accepted your teleport request being teleported now...");
                teleportAccepted.teleport(player);
                API.lasttpa.remove(player.getUniqueId());
            }
        }
        return true;
    }
}
