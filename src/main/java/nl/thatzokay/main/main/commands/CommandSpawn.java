package nl.thatzokay.main.main.commands;

import nl.thatzokay.main.main.Main;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandSpawn implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (command.getName().equalsIgnoreCase("spawn")){
            if (commandSender instanceof Player){
                Player player = (Player) commandSender;
                Main main = Main.getInstance();

                if (main.connection.isSpawnLocationSet()){
                    Location spawnLocation = main.connection.getSpawnLocation();
                    player.sendMessage("teleporting to spawn....");
                    player.teleport(spawnLocation);
                }else{
                    player.sendMessage("Please set spawn location.");
                }
            }
        }
        return true;
    }
}
