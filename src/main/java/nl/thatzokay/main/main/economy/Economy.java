package nl.thatzokay.main.main.economy;

import nl.thatzokay.main.main.Main;
import nl.thatzokay.main.main.sql.MySQLConnection;
import org.bukkit.entity.Player;

import java.sql.PreparedStatement;
import java.sql.SQLException;


public class Economy {

    private Main plugin;

    private Player player;

    private String playerUUID;

    private MySQLConnection connection;

    public Economy(Player player, Main plugin) {
        this.player = player;
        this.plugin = plugin;
        this.playerUUID = player.getUniqueId().toString();
        this.connection = plugin.connection;
    }



    public double getBalance(){
        double balance = 0;
        try {
            PreparedStatement getBalance = connection.getConnection().prepareStatement("SELECT `BALANCE` FROM `players` WHERE `UUID` = '" + playerUUID + "';");
            balance = getBalance.getResultSet().getDouble("BALANCE");

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return balance;
    }

    public void addMoney(double amount){
        double balance = getBalance();
        balance = balance + amount;
        try {
            PreparedStatement setBalance = connection.getConnection().prepareStatement("UPDATE `players` SET `BALANCE` = "+ balance + " WHERE `UUID` = '" + playerUUID + "';");
            setBalance.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void removeMoney(double amount){
        double balance = getBalance();
        balance = balance - amount;
        try {
            PreparedStatement setBalance = connection.getConnection().prepareStatement("UPDATE `players` SET `BALANCE` = "+ balance + " WHERE `UUID` = '" + playerUUID + "';");
            setBalance.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
