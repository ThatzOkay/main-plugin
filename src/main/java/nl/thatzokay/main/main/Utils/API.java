package nl.thatzokay.main.main.Utils;


import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.util.HashMap;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class API {

    private String currentDate;

    public API(){
        currentDate = java.time.LocalDate.now().toString();
    }

    private final Pattern hexPattern = Pattern.compile("#[A-Fa-f0-9]{6}");

    public String getCurrentDate(){
        return currentDate;
    }

    public String format(String m){
        if (Bukkit.getVersion().contains("1.16")){
            //hex colors
            Matcher matcher = hexPattern.matcher(m);
            while(matcher.find()){
                String color = m.substring(matcher.start(), matcher.end());
                m = m.replace(color, ChatColor.of(color) + "");
                matcher = hexPattern.matcher(m);
            }
        }
        return ChatColor.translateAlternateColorCodes('&' , m);
    }

    public static HashMap<UUID,UUID> lastsend = new HashMap<>();

    public static HashMap<UUID,UUID> lasttpa = new HashMap<>();


    public String locationToString(Location location){

        double x = Math.round(location.getX()* 100.0) / 100.0;
        double y = Math.round(location.getY()* 100.0) / 100.0;;
        double z = Math.round(location.getZ()* 100.0) / 100.0;;
        double yaw = Math.round(location.getYaw()* 100.0) / 100.0;;
        double pitch = Math.round(location.getPitch()* 100.0) / 100.0;;
        String worldName = location.getWorld().getName();
       return worldName + " " + x + " " + y + " " + z + " " + yaw + " " + pitch;
    }

    public boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}
