package nl.thatzokay.main.main.sql;

import com.google.gson.internal.$Gson$Preconditions;
import nl.thatzokay.main.main.Main;
import nl.thatzokay.main.main.Utils.API;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerQuitEvent;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;

public class MySQLConnection {

    private static Connection connection;
    private static Main plugin = Main.getInstance();
    private String host;
    private int port;
    private String username;
    private String password;
    private String database;

    private static String table_prefix = plugin.getConfig().getString("TablePrefix");


    public MySQLConnection(String host, int port, String username, String password, String database){
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;
        this.database = database;
        try{
        synchronized (this){
            if(getConnection() != null && !getConnection().isClosed()){
                return;
            }

            Class.forName("com.mysql.jdbc.Driver");
            setConnection( DriverManager.getConnection("jdbc:mysql://" + this.host + ":"
                    + this.port + "/" + this.database, this.username, this.password));

            Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN + "MYSQL CONNECTED");
            createPlayerTable();
            createHometable();
            createSystemTable();
            createMessagesTable();
        }
    }catch(SQLException e){
        e.printStackTrace();
    }catch(ClassNotFoundException e){
        e.printStackTrace();
    }
    }



    public static Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public void createMessagesTable(){
        try{
            PreparedStatement createMessagesTable = getConnection().prepareStatement("CREATE TABLE IF NOT EXISTS `"+table_prefix+"_messages` (" +
                    "  `NAME` VARCHAR(255) NOT NULL," +
                    "  `MESSAGE` LONGTEXT NOT NULL," +
                    "  PRIMARY KEY (`NAME`));");
            PreparedStatement insertMessagesTable = getConnection().prepareStatement("INSERT INTO `"+table_prefix+"_messages`" +
                    "(`NAME`," +
                    "`MESSAGE`)" +
                    "VALUES" +
                    "('NOPERMISSIONS'," +
                    "\"#135465You don't have permissions to use this command\"),('PLAYEROFFLINE',\"#e9fa00%offline_player% is offline.\");");
            createMessagesTable.executeUpdate();
            createMessagesTable.close();
            if (!messagescreated()) {
                insertMessagesTable.executeUpdate();
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
    public boolean messagescreated(){
        try{
            PreparedStatement statement = getConnection().prepareStatement("SELECT * FROM `"+table_prefix+"_messages`");
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()){
                return true;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    public String getMessage(String messageName){
        String message = null;
        try{
            PreparedStatement statement = getConnection().prepareStatement("SELECT `MESSAGE` FROM `"+table_prefix+"_messages` WHERE `NAME` ='" + messageName + "';");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                message = resultSet.getString(1);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return message;
    }

    public static List getMessageNames(){
        List<String> messages = null;
        try{
            PreparedStatement statement = getConnection().prepareStatement("SELECT * FROM `"+table_prefix+"_messages`");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                String messageName = resultSet.getString(1);
                messages.add(messageName);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return messages;
    }

    public boolean systemCreated(){
        try{
            PreparedStatement statement = getConnection().prepareStatement("SELECT * FROM `"+table_prefix+"_system`");
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()){
                return true;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    private void createSystemTable() {
        try{
            PreparedStatement createSystemTable = getConnection().prepareStatement("CREATE TABLE IF NOT EXISTS `"+table_prefix+"_system` (" +
                    "  `LANG` VARCHAR(5) NOT NULL DEFAULT 'EN_US'," +
                    "  `JOINED_PLAYERS` INT NOT NULL DEFAULT 0," +
                    "  `SERVER_BALANCE` DOUBLE NOT NULL DEFAULT 0," +
                    "  `ONLINE_PLAYERS` INT NOT NULL DEFAULT 0," +
                    "  `SPAWN_LOCATION` VARCHAR(45));");
            PreparedStatement inserSystemTable = getConnection().prepareStatement("INSERT INTO `"+table_prefix+"_system` (`SPAWN_LOCATION`) VALUES (NULL);");
            createSystemTable.executeUpdate();
            if (!systemCreated()) {
                inserSystemTable.executeUpdate();
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public boolean isSpawnLocationSet(){
        try {
            PreparedStatement statement = getConnection()
                    .prepareStatement("SELECT * FROM "+table_prefix+"_system WHERE SPAWN_LOCATION = null");

            ResultSet results = statement.executeQuery();
            if (results.getString(1) == null) {
                return false;
            }else{
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public void setSpawnLocation(Location location){
        API api = new API();
        String spawnLocation = api.locationToString(location);
        try {
            PreparedStatement setSpawnLocation = getConnection().prepareStatement("UPDATE `"+table_prefix+"_system` SET `SPAWN_LOCATION` = ?");
            setSpawnLocation.setString(1, spawnLocation);
            setSpawnLocation.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Location getSpawnLocation(){
        Location location = null;
        try {
            PreparedStatement getHomeLocation = getConnection().prepareStatement("SELECT `SPAWN_LOCATION` FROM `"+table_prefix+"_system`;");
            ResultSet resultSet = getHomeLocation.executeQuery();
            while (resultSet.next()) {
                String strLocation = resultSet.getString(1);
                //player.sendMessage(strLocation);
                String loc[] = strLocation.split(" ");
                String world = loc[0];
                double x = Double.parseDouble(loc[1]);
                double y = Double.parseDouble(loc[2]);
                double z = Double.parseDouble(loc[3]);
                float yaw = Float.parseFloat(loc[4]);
                float pitch = Float.parseFloat(loc[5]);

                location = new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return location;
    }

    public void setOnlinePlayers(){
        try{
            PreparedStatement addOnlinePlayer = getConnection().prepareStatement("UPDATE `"+table_prefix+"_system`" +
                    "SET" +
                    "`ONLINE_PLAYERS` = '"+ Bukkit.getOnlinePlayers().size() +"';");
            addOnlinePlayer.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public boolean playerExists(Player player) {
        UUID uuid = player.getUniqueId();
        try {
            PreparedStatement statement = getConnection()
                    .prepareStatement("SELECT * FROM "+table_prefix+"_players WHERE UUID=?");
            statement.setString(1, uuid.toString());
            ResultSet results = statement.executeQuery();
            if (results.next()) {
                return true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void createPlayerTable(){
        try {
            PreparedStatement createPlayerTable = getConnection().prepareStatement("CREATE TABLE IF NOT EXISTS `"+table_prefix+"_players` (" +
                    "  `UUID` VARCHAR(36) NOT NULL," +
                    "  `NAME` VARCHAR(45) NOT NULL," +
                    "  `BALANCE` DOUBLE NULL," +
                    "  `JOINDATE` DATE NULL," +
                    "  `LEAVEDATE` DATE NULL," +
                    "  PRIMARY KEY (`UUID`));");
            createPlayerTable.executeUpdate();
            createPlayerTable.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void createHometable(){
        try {
            PreparedStatement createHomeTable = getConnection().prepareStatement("CREATE TABLE IF NOT EXISTS  `"+table_prefix+"_homes` (`ID` INT NOT NULL AUTO_INCREMENT," +
                    "`NAME` VARCHAR(45) NOT NULL," +
                    "`LOCATION` VARCHAR(45) NOT NULL," +
                    "`UUID` VARCHAR(36) NOT NULL," +
                    "PRIMARY KEY (`ID`));");
            createHomeTable.executeUpdate();
            createHomeTable.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public java.util.Date getLeaveDate(Player player){
        UUID uuid = player.getUniqueId();
        java.util.Date date = null;
        try {
            PreparedStatement getLeaveDate = getLeaveDate = getConnection().prepareStatement("SELECT LEAVEDATE FROM "+table_prefix+"_players WHERE UUID =?;");
            getLeaveDate.setString(1, uuid.toString());
            ResultSet resultSet = getLeaveDate.executeQuery();
            while (resultSet.next()) {
                date = resultSet.getDate(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return date;
    }

    public void setLeaveDate(Player player){
        Date leaveDate = java.sql.Date.valueOf(LocalDate.now());
        UUID uuid = player.getUniqueId();
        try {
            PreparedStatement updateLeaveDate = getConnection().prepareStatement("UPDATE "+table_prefix+"_players SET LEAVEDATE=? WHERE UUID = ?;");
            updateLeaveDate.setDate(1, leaveDate);
            updateLeaveDate.setString(2, uuid.toString());
            updateLeaveDate.executeUpdate();
            updateLeaveDate.close();
        }catch (SQLException throwables){
            throwables.printStackTrace();
        }
    }

    public void createPlayer(Player player) {
        UUID uuid = player.getUniqueId();
        Date date = java.sql.Date.valueOf(LocalDate.now());

        try {
            if (!playerExists(player)) {
                PreparedStatement insert = getConnection().prepareStatement("INSERT INTO "+table_prefix+"_players (UUID,NAME,BALANCE,JOINDATE,LEAVEDATE) VALUES (?,?,?,?,?)");
                insert.setString(1, uuid.toString());
                insert.setString(2, player.getName());
                insert.setDouble(3, 100);
                insert.setDate(4, date);
                insert.setDate(5, null);
                insert.executeUpdate();
                insert.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void closeConnection(){
        try {
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }


    public void createNewHome(String homeName, Player player, Location location){
            try {
                double x = Math.round(location.getX()* 100.0) / 100.0;
                double y = Math.round(location.getY()* 100.0) / 100.0;;
                double z = Math.round(location.getZ()* 100.0) / 100.0;;
                double yaw = Math.round(location.getYaw()* 100.0) / 100.0;;
                double pitch = Math.round(location.getPitch()* 100.0) / 100.0;;
                String worldName = location.getWorld().getName();
                String loc = worldName + " " + x + " " + y + " " + z + " " + yaw + " " + pitch;
                PreparedStatement insertHome = getConnection().prepareStatement("INSERT INTO `"+table_prefix+"_homes` (NAME,LOCATION,UUID) VALUES (?,?,?)");
                insertHome.setString(1, homeName);
                insertHome.setString(2, loc);
                insertHome.setString(3, player.getUniqueId().toString());
                insertHome.executeUpdate();
                insertHome.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
    }

    public boolean doesHomeExist(String homeName,  Player player){
        try {
            PreparedStatement getRows = getConnection().prepareStatement("SELECT * FROM `"+table_prefix+"_homes` WHERE `NAME` = ? && `UUID` = ?;");
            getRows.setString(1, homeName);
            getRows.setString(2, player.getUniqueId().toString());
            ResultSet results = getRows.executeQuery();
            if (results.next()) {
                results.close();
                return true;
            }else{
                results.close();
                return false;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void updateHome(String homeName, Player player, Location location) {
        try {
            double x = Math.round(location.getX()* 100.0) / 100.0;
            double y = Math.round(location.getY()* 100.0) / 100.0;;
            double z = Math.round(location.getZ()* 100.0) / 100.0;;
            double yaw = Math.round(location.getYaw()* 100.0) / 100.0;;
            double pitch = Math.round(location.getPitch()* 100.0) / 100.0;;
            String worldName = location.getWorld().getName();
            String loc = worldName + " " + x + " " + y + " " + z + " " + yaw + " " + pitch;
            PreparedStatement updateHome = getConnection().prepareStatement("UPDATE `"+table_prefix+"_homes` SET `NAME` = ?, `LOCATION` = ? WHERE `NAME` = ? && `UUID` = ?;");
            updateHome.setString(1, homeName);
            updateHome.setString(2, loc);
            updateHome.setString(3, homeName);
            updateHome.setString(4, player.getUniqueId().toString());
            updateHome.executeUpdate();
            updateHome.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Location getHomeLocation(Player player, String homeName){
        Location location = null;
        try {
            PreparedStatement getHomeLocation = getConnection().prepareStatement("SELECT `LOCATION` FROM `"+table_prefix+"_homes` WHERE `UUID` = ? && `NAME` = ?;");
            getHomeLocation.setString(1, player.getUniqueId().toString());
            getHomeLocation.setString(2, homeName);
            ResultSet resultSet = getHomeLocation.executeQuery();
            while (resultSet.next()) {
                String strLocation = resultSet.getString(1);
                //player.sendMessage(strLocation);
                String loc[] = strLocation.split(" ");
                String world = loc[0];
                double x = Double.parseDouble(loc[1]);
                double y = Double.parseDouble(loc[2]);
                double z = Double.parseDouble(loc[3]);
                float yaw = Float.parseFloat(loc[4]);
                float pitch = Float.parseFloat(loc[5]);

                location = new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return location;
    }

    public List<String> getPlayerHomeNames(Player player){
        List<String> homes = new ArrayList<>();
        try {
            PreparedStatement getHomes = getConnection().prepareStatement("SELECT `NAME` FROM `"+table_prefix+"_homes` WHERE `UUID` = ?");
            getHomes.setString(1, player.getUniqueId().toString());
            ResultSet resultSet = getHomes.executeQuery();
            ResultSetMetaData resultSetMetaData = getHomes.getMetaData();
            int columnCount = resultSetMetaData.getColumnCount();
            while (resultSet.next()) {
                int i = 1;
                while(i <= columnCount) {
                    homes.add(resultSet.getString(i++));
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return homes;
    }
}
