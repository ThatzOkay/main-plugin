package nl.thatzokay.main.main.events;

import me.clip.placeholderapi.PlaceholderAPI;
import nl.thatzokay.main.main.Main;
import nl.thatzokay.main.main.Utils.API;
import nl.thatzokay.main.main.sql.MySQLConnection;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerBedEnterEvent;

public class OnplayerEnterBed implements Listener {

    @EventHandler
    public void onplayerEnterBed(PlayerBedEnterEvent event){
        Player player = event.getPlayer();
        World world = player.getWorld();
        Location bedHomeLocation = player.getLocation();
        Main main = Main.getInstance();
        MySQLConnection mySQLConnection = main.connection;
        API api = new API();

        if(!mySQLConnection.doesHomeExist("bed", player)){
            mySQLConnection.createNewHome("bed", player, bedHomeLocation);
        }else{
            mySQLConnection.updateHome("bed", player, bedHomeLocation);
        }
        if (world.getTime() > 12542) {
            main.getServer().broadcastMessage(PlaceholderAPI.setPlaceholders(player,api.format("%player_name% is now sleeping")));
            main.getServer().getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
                public void run() {
                    world.setTime(0);
                    event.setCancelled(true);
                }
            }, 20 * 1);
        }
    }
}
