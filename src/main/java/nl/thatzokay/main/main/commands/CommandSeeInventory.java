package nl.thatzokay.main.main.commands;

import nl.thatzokay.main.main.Main;
import nl.thatzokay.main.main.Utils.API;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandSeeInventory implements CommandExecutor{
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (command.getName().equalsIgnoreCase("seeinventory")){
            if (commandSender instanceof Player){
                API api = new API();
                Player player = (Player) commandSender;
                Player p2 = Bukkit.getPlayer(strings[0]);
                if (strings.length == 0){
                    player.sendMessage("");
                }else{
                    if (player.hasPermission("main.command.seeinventory") || player.isOp()){
                        if (p2 != null) {
                            player.openInventory(p2.getInventory());
                        }else{
                            String message = Main.getInstance().connection.getMessage("PLAYEROFFLINE").replace("%offline_player%", strings[0]);
                            player.sendMessage(api.format(message));
                        }
                    }else{
                        String message = Main.getInstance().connection.getMessage("NOPERMISSIONS");
                        player.sendMessage(api.format(message));
                    }
                }
            }
        }
        return true;
    }
}
