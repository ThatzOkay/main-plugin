package nl.thatzokay.main.main.commands;

import nl.thatzokay.main.main.Main;
import nl.thatzokay.main.main.Utils.API;
import nl.thatzokay.main.main.inventory.SettingsInventory;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandSettings implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (command.getName().equalsIgnoreCase("settings")){
            if (commandSender instanceof Player){
                Player player = (Player) commandSender;
                API api = new API();
                if (player.hasPermission("main.settings") || player.isOp()){
                    SettingsInventory settingsInventory = new SettingsInventory();
                    settingsInventory.SettingsInventory(player);
                }else{
                    String message = Main.getInstance().connection.getMessage("NOPERMISSIONS");
                    player.sendMessage(api.format(message));
                }
            }
        }
        return true;
    }
}
