package nl.thatzokay.main.main.commands;

import nl.thatzokay.main.main.Main;
import nl.thatzokay.main.main.Utils.API;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandSetSpawn implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (command.getName().equalsIgnoreCase("setspawn")){
            if (commandSender instanceof Player){
                Player player = (Player) commandSender;
                API api = new API();
                if (player.hasPermission("main.setspawn") || player.isOp()) {
                    Location spawnLocation = player.getLocation();
                    Main main = Main.getInstance();
                    main.connection.setSpawnLocation(spawnLocation);
                    player.sendMessage("Spawn location set!");
                }else{
                    String message = Main.getInstance().connection.getMessage("NOPERMISSIONS");
                    player.sendMessage(api.format(message));
                }
            }
        }
        return true;
    }
}
