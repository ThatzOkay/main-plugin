package nl.thatzokay.main.main;

import nl.thatzokay.main.main.commands.*;
import nl.thatzokay.main.main.config.ConfigManager;
import nl.thatzokay.main.main.events.OnInventoryClick;
import nl.thatzokay.main.main.events.OnPlayerJoinEvent;
import nl.thatzokay.main.main.events.OnPlayerLeaveEvent;
import nl.thatzokay.main.main.events.OnplayerEnterBed;
import nl.thatzokay.main.main.sql.MySQLConnection;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public final class Main extends JavaPlugin {

    private static Main instance;

    private YamlConfiguration configuration;
    private ConfigManager config;

    public MySQLConnection connection;

    public static Main getInstance(){
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;
        if (!getDataFolder().exists()) {
            getDataFolder().mkdir();
        }
        config = new ConfigManager("", "config.yml", this);
        setupConfig();

        configuration = config.getConfiguration();
        int port = configuration.getInt("DBPort");
        String host = configuration.getString("DBHost");
        String username = configuration.getString("DBUsername");
        String password = configuration.getString("DBPass");
        String database = configuration.getString("DBName");

        this.connection = new MySQLConnection(host, port, username, password, database);

        Bukkit.getPluginManager().registerEvents(new OnPlayerJoinEvent(), this);
        Bukkit.getPluginManager().registerEvents(new OnplayerEnterBed(), this);
        Bukkit.getPluginManager().registerEvents(new OnPlayerLeaveEvent(), this);
        Bukkit.getPluginManager().registerEvents(new OnInventoryClick(), this);

        getCommand("homes").setExecutor(new CommandHomes());
        getCommand("sethome").setExecutor(new CommandSetHome());
        getCommand("home").setExecutor(new CommandHome());
        getCommand("setspawn").setExecutor(new CommandSetSpawn());
        getCommand("spawn").setExecutor(new CommandSpawn());
        getCommand("onlineplayers").setExecutor(new CommandOnlinePlayers());
        getCommand("message").setExecutor(new CommandMessage());
        getCommand("replay").setExecutor(new CommandReplay());
        getCommand("tpa").setExecutor(new CommandTPA());
        getCommand("tpaccept").setExecutor(new CommandTPAccept());
        getCommand("tpdeny").setExecutor(new CommandTPDeny());
        getCommand("seeinventory").setExecutor(new CommandSeeInventory());
        getCommand("enderchest").setExecutor(new CommandEnderChest());
        getCommand("workbench").setExecutor(new CommandWorkBench());
        getCommand("playerweather").setExecutor(new CommandPlayerWeather());
        getCommand("playertime").setExecutor(new CommandPlayerTime());
        getCommand("currenttime").setExecutor(new CommandCurrentTime());
        getCommand("settings").setExecutor(new CommandSettings());



    }

    @Override
    public void onDisable() {
        connection.closeConnection();
    }

    private void setupConfig(){
        FileReader fr = null;
        try {
            fr = new FileReader(config.getFile());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            if(fr.read() == -1){
                this.configuration = config.getConfiguration();

                configuration.set("DBPort" , 3306);
                configuration.set("DBHost" , "localhost");
                configuration.set("DBUsername" , "");
                configuration.set("DBPass" , "");
                configuration.set("DBName" , "");
                configuration.set("TablePrefix", "main");

                config.saveConfig();
            }else this.configuration = config.getConfiguration();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
