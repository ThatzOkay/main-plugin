package nl.thatzokay.main.main.commands;

import nl.thatzokay.main.main.Main;
import nl.thatzokay.main.main.Utils.API;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.logging.Level;

public class CommandTPA implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (command.getName().equalsIgnoreCase("tpa")){
            if (commandSender instanceof Player){
                Main main = Main.getInstance();
                API api = new API();
                Player player = (Player) commandSender;
                Player sendTo = Bukkit.getPlayer(strings[0]);

                if (strings.length == 0){
                    return true;
                }

                if (sendTo == null){
                    String message = Main.getInstance().connection.getMessage("PLAYEROFFLINE").replace("%offline_player%", strings[0]);
                    player.sendMessage(api.format(message));
                }else if (sendTo == player){
                    player.sendMessage(api.format("§cYou can't teleport to yourself"));
                }else{
                    sendTo.sendMessage(api.format(player.getDisplayName() + " has send you a teleport request\n" +
                            "you have 120 second to accept or deny this\n" +
                            "/tpaccept to accept or /tpdeny to deny"));
                    API.lasttpa.put(sendTo.getUniqueId(), player.getUniqueId());
                    main.getServer().getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
                        public void run() {
                            API.lasttpa.remove(sendTo.getUniqueId());
                        }
                    }, 20 * 120);
                }

            }
        }
        return true;
    }
}
