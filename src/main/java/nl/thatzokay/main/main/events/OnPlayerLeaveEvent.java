package nl.thatzokay.main.main.events;

import nl.thatzokay.main.main.Main;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;


public class OnPlayerLeaveEvent implements Listener {

    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent event){
        Player player = event.getPlayer();
        Main main = Main.getInstance();
        main.connection.setLeaveDate(player);
    }

}
