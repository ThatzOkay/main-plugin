package nl.thatzokay.main.main.commands;

import nl.thatzokay.main.main.Main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommandHomes implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (command.getName().equalsIgnoreCase("homes")){
            if (commandSender instanceof Player){
                Player player = (Player) commandSender;
                Main plugin = Main.getInstance();
                List homes = plugin.connection.getPlayerHomeNames(player);
                player.sendMessage(Arrays.toString(homes.toArray()));
            }
        }
        return true;
    }
}
