package nl.thatzokay.main.main.inventory;

import com.mysql.jdbc.authentication.MysqlClearPasswordPlugin;
import nl.thatzokay.main.main.Main;
import nl.thatzokay.main.main.Utils.API;
import nl.thatzokay.main.main.sql.MySQLConnection;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

public class EditmessagesInventory {

    private static Main main = Main.getInstance();

    private static API api = new API();

    private static List messagesNames = MySQLConnection.getMessageNames();

    public static Inventory inventory = main.getServer().createInventory(null,54,api.format("Edit Messagges"));

    public static void EditmessagesInventory(Player player){
        inventory.clear();
        setItems();
        player.openInventory(inventory);
    }

    public static void setItems(){
        for (int i = 0; i > messagesNames.size(); i++){
            ItemStack item = new ItemStack(Material.WRITTEN_BOOK);
            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(String.valueOf(messagesNames.get(i)));
            item.setItemMeta(itemMeta);
            inventory.setItem(i + 1, item);
        }
    }
}
