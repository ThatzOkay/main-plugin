package nl.thatzokay.main.main.events;

import nl.thatzokay.main.main.Utils.API;
import nl.thatzokay.main.main.inventory.EditmessagesInventory;
import nl.thatzokay.main.main.inventory.SettingsInventory;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;

import java.lang.invoke.SwitchPoint;

public class OnInventoryClick implements Listener {

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event){
        Player player = (Player) event.getWhoClicked();
        API api = new API();
        InventoryView inventoryView = event.getView();
        ItemStack clickedItem = event.getCurrentItem();

        if (inventoryView.getTitle().equalsIgnoreCase(api.format("Settings"))){
            event.setCancelled(true);
            if(clickedItem == null){
                return;
            }
            if (clickedItem.getType() == Material.BOOK){
                EditmessagesInventory.EditmessagesInventory(player);
            }

        }
    }
}
