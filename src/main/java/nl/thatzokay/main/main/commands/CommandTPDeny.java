package nl.thatzokay.main.main.commands;

import nl.thatzokay.main.main.Utils.API;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandTPDeny implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (command.getName().equalsIgnoreCase("tpdeny")){
            if (commandSender instanceof Player){
                API api = new API();
                Player player = (Player) commandSender;
                Player teleportTo = Bukkit.getPlayer(api.lasttpa.get(player.getUniqueId()));

                if (teleportTo == null){
                    player.sendMessage("The player has gone offline");
                    api.lasttpa.remove(player.getUniqueId());
                }

                player.sendMessage(teleportTo.getDisplayName() + " has denied your teleport request");
                api.lasttpa.remove(player.getUniqueId());
            }
        }
        return true;
    }
}
