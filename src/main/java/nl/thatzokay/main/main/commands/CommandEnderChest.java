package nl.thatzokay.main.main.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandEnderChest implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (command.getName().equalsIgnoreCase("enderchest")){
            if (commandSender instanceof Player){
                Player player = (Player) commandSender;
                if (player.hasPermission("main.command.enderchest")){
                    player.openInventory(player.getEnderChest());
                }else{

                }
            }
        }
        return true;
    }
}
