package nl.thatzokay.main.main.commands;

import nl.thatzokay.main.main.Main;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CommandHome implements TabExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (command.getName().equalsIgnoreCase("home")) {
            if (commandSender instanceof Player) {
                Player player = (Player) commandSender;
                Main plugin = Main.getInstance();
                if (strings.length == 1) {
                    String homeName = strings[0];
                    if (plugin.connection.doesHomeExist(homeName, player)) {
                        Location homeLocation = plugin.connection.getHomeLocation(player, homeName);
                        player.teleport(homeLocation);
                    } else {
                        player.sendMessage("this home does not exist");
                    }
                } else {
                    if (plugin.connection.doesHomeExist("home", player)) {
                        Location homeLocation = plugin.connection.getHomeLocation(player, "home");
                        player.teleport(homeLocation);
                    } else {
                        player.sendMessage("this home does not exist");
                    }
                }

            }
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] strings){
        Player player = (Player) commandSender;
        Main plugin = Main.getInstance();
        return  plugin.connection.getPlayerHomeNames(player);
    }

}
