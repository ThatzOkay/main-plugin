package nl.thatzokay.main.main.commands;

import nl.thatzokay.main.main.Main;
import nl.thatzokay.main.main.Utils.API;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandSetHome implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (command.getName().equalsIgnoreCase("sethome")){
            if (commandSender instanceof Player){
                Player player = (Player) commandSender;
                Main plugin = Main.getInstance();
                if (strings.length == 1){
                    String homeName = strings[0];
                    if(!plugin.connection.doesHomeExist(homeName,player)){
                        player.sendMessage(homeName + " home created");
                        plugin.connection.createNewHome(homeName, player, player.getLocation());
                    }else{
                        player.sendMessage(homeName + " home updated");
                        plugin.connection.updateHome(homeName, player, player.getLocation());
                    }
                }else{
                    if(!plugin.connection.doesHomeExist("home",player)){
                        player.sendMessage("Home home created");
                        plugin.connection.createNewHome("home", player, player.getLocation());
                    }else{
                        player.sendMessage("Home home updated");
                        plugin.connection.updateHome("home", player, player.getLocation());
                    }
                }
            }
        }
        return true;
    }
}
