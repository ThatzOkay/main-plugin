package nl.thatzokay.main.main.commands;

import nl.thatzokay.main.main.Main;
import nl.thatzokay.main.main.Utils.API;
import org.bukkit.command.*;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ForkJoinPool;

public class CommandPlayerTime implements TabExecutor {

    String[] attributes = {"set", "add", "reset"};
    String[] times = {"day", "midnight", "night", "noon"};

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (command.getName().equalsIgnoreCase("playertime")){
            if (commandSender instanceof Player) {
                Player player = (Player) commandSender;
                API api = new API();
                if (player.hasPermission("main.command.playertime") || player.isOp()) {
                    if (strings.length == 0) {
                        player.sendMessage("Must be add or set [number]");
                    }
                    if (strings[0].equalsIgnoreCase("set")) {
                        if (strings.length == 2) {
                            if (strings[1].equalsIgnoreCase("day")){
                                player.setPlayerTime(1000, false);
                                player.sendMessage("time set to 1000");
                            }else if (strings[1].equalsIgnoreCase("midnight")){
                                player.setPlayerTime(18000, false);
                                player.sendMessage("time set to 18000");
                            }else if (strings[1].equalsIgnoreCase("night")){
                                player.setPlayerTime(13000, false);
                                player.sendMessage("time set to 13000");
                            }else if (strings[1].equalsIgnoreCase("noon")){
                                player.setPlayerTime(6000, false);
                                player.sendMessage("time set to 6000");
                            }else if (api.isNumeric(strings[1])){
                                player.setPlayerTime(Integer.parseInt(strings[1]), false);
                                player.sendMessage("time set to "+ Integer.parseInt(strings[1]));
                            }else{
                                player.sendMessage("must be a number");
                            }
                        } else {

                        }
                    } else if (strings[0].equalsIgnoreCase("add")) {
                        if (strings.length == 2) {
                            if (api.isNumeric(strings[1])){
                                long currentTime = player.getPlayerTime();
                                long time = currentTime + Integer.parseInt(strings[1]);
                                player.setPlayerTime(time, false);
                                player.sendMessage("Player time set to "+ time);
                            }else{
                                player.sendMessage("must be a number");
                            }
                        } else {

                        }
                    }else if (strings[0].equalsIgnoreCase("reset")){
                        player.resetPlayerTime();
                    }
                }else{
                    String message = Main.getInstance().connection.getMessage("NOPERMISSIONS");
                    player.sendMessage(api.format(message));
                }
            }
        }

        return true;
    }
    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] strings) {
        if (strings.length == 1){
            return Arrays.asList(attributes);
        }
        if (strings[0].equalsIgnoreCase("set")){
            if (strings.length == 2){
            return Arrays.asList(times);
            }
        }
        return null;
    }
}
