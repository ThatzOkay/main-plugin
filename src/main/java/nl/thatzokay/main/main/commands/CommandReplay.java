package nl.thatzokay.main.main.commands;

import nl.thatzokay.main.main.Utils.API;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandReplay implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (command.getName().equalsIgnoreCase("replay")){
            if (commandSender instanceof Player){
                Player player = (Player) commandSender;
                Player sendTo = Bukkit.getPlayer(API.lastsend.get(player.getUniqueId()));
                if (sendTo == null){
                    player.sendMessage("&cThe last player you messaged is offline.");
                    API.lastsend.remove(player.getUniqueId());
                } else if(strings.length == 0){
                    player.sendMessage("§cusage: /r [message]");
                } else{
                    String msg = "";

                    for(int i = 0; i < strings.length; ++i) {
                        msg = msg + " " + strings[i];
                    }

                    msg = msg.replace("&", "§");
                    player.sendMessage(player.getDisplayName() + "§8 >> " + sendTo.getDisplayName() + "§8 >>§7" + msg);
                    sendTo.sendMessage("From: " + player.getDisplayName() + "§8 >>§7" + msg);
                }


            }
        }
        return true;
    }
}
