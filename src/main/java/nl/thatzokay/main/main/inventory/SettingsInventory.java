package nl.thatzokay.main.main.inventory;

import nl.thatzokay.main.main.Main;
import nl.thatzokay.main.main.Utils.API;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class SettingsInventory {

    private static Main main = Main.getInstance();

    private static API api = new API();

    public static ItemStack editMessages;
    public static ItemMeta editmessagesMeta;

    public static Inventory inventory = main.getServer().createInventory(null,9,api.format("Settings"));

    public static void SettingsInventory(Player player){
        inventory.clear();
        setItemStack();
        inventory.addItem(editMessages);
        player.openInventory(inventory);
    }

    public static void setItemStack(){
            editMessages = new ItemStack(Material.BOOK,1);
            editmessagesMeta = editMessages.getItemMeta();
            setItemMeta();
    }

    public static void setItemMeta(){
        editmessagesMeta.setDisplayName("Edit Messages");
        editMessages.setItemMeta(editmessagesMeta);
    }
}
